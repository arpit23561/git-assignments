# Topics

## Assignment1: 
* Initialize a directory as git repository.

#### solution 
            git init

* Create a blank files like a.txt, b.txt.

#### solution 
            touch a.txt b.txt
* Write "Hi Team Ninja" into a.txt

#### solution 
            echo "Hi Team Ninja" > a.txt
* Write "This is VCS:Git" into b.txt

#### solution 
            echo "This is VCS:Git" > b.txt
* Set git config variables.

#### solution 
            git config --global user.name "arpit2356"
            git config --global user.mail "arpit2356"
            git config --global user.password "XXXXX"
* Commit both files.

#### solution 
            git add *
        
* Create a branch **ninja**

#### solution 
            git branch ninja
* Switch into branch **ninja**

#### solution 
            git checkout ninja
* Update file b.txt with ""This is VCS:Git. This is ninja branch"

#### solution 
            echo ""This is VCS:Git. This is ninja branch" > b.txt
* Check for the difference made in the file.

#### solution 
            git diff b.txt
* Commit your changes in ninja branch.

#### solution 
            git add b.txt
            git commit -m "first ninja commit"
* Check difference between last two commits.

#### solution
            git diff HEAD~2
* Rename your file b.txt to c.txt

#### solution 
            mv b.txt c.txt
* Commit your changes.

#### solution
            git add c.txt
            git commit -m "second ninga commit"
* Remove file c.txt.

#### solution 
            rm -f c.txt
* Commit your changes.

#### solution 
            git commit -m "third ninga commit"
* Create a file text.txt and add it. 

#### solution 
            touch text.txt && git add text.txt
* Remove it from the staging area.
 
#### solution 
            git reset HEAD text.txt

===================================================================================================================================

## Assignment2: 
* Create a account on github.com.

![screenshot](./githubacc.png)
* Fork [ot-training/jenkins](https://github.com/ot-training/jenkins.git) repo into your account.
* Clone your repo into your machine.
                           
                            git clone git@github.com:arpit2356/jenkins.git
===================================================================================================================================

## Assignment3: 
* Clone your repo into your machine.
                                    
                                    git clone git@github.com:arpit2356/jenkins.git
* Add a file to the attendees directory that describes you (ex. filename wouild be your name like *saurabh.vajpayee.txt*).

                                    touch arpit.srivastava.txt
* Commit your changes. Write a meaningful message like "Added information about YOUR NAME HERE."

                                    git commit -m "added arpit.srivastava.txt file"
* Save your changes into remote repo.

                                    git push jenkins master
* Check list of changes in your repo. 

                                    git log
* Add your name into attendees/assignments/day1/attendees.md

                                    echo "Arpit Srivastava" >> attendees/assignments/day1/attendees.md

